package com.company;

import java.util.Scanner;
import java.lang.Math;
import java.lang.String;
import java.util.concurrent.TimeUnit;

class Distance {
    private static Integer[][] matrix = null;

    public static int recursiveLevenshtein(String string1, String string2) {
        if (string1.length() == 0 || string2.length() == 0) {
            return Math.abs(string1.length() - string2.length());
        }
        int cost = (string1.charAt(string1.length() - 1) == string2.charAt(string2.length() - 1)) ? 0 : 1;
        return Math.min(Math.min(recursiveLevenshtein(string1, string2.substring(0, string2.length() - 1)) + 1,
                recursiveLevenshtein(string1.substring(0, string1.length() - 1), string2) + 1),
                recursiveLevenshtein(string1.substring(0, string1.length() - 1),
                        string2.substring(0, string2.length() - 1)) + cost);
    }

    public static int recursiveMatrixLevenshtein(String string1, String string2) {
        if (matrix[string1.length()][string2.length()] != null) {
            return matrix[string1.length()][string2.length()];
        }
        if (string1.length() == 0 || string2.length() == 0) {
            matrix[string1.length()][string2.length()] = Math.abs(string1.length() - string2.length());
            return matrix[string1.length()][string2.length()];
        }
        int cost = (string1.charAt(string1.length() - 1) == string2.charAt(string2.length() - 1)) ? 0 : 1;
        matrix[string1.length()][string2.length()] = Math.min(Math.min(recursiveMatrixLevenshtein(string1,
                string2.substring(0, string2.length() - 1)) + 1,
                recursiveMatrixLevenshtein(string1.substring(0, string1.length() - 1), string2) + 1),
                recursiveMatrixLevenshtein(string1.substring(0, string1.length() - 1),
                        string2.substring(0, string2.length() - 1)) + cost);
        return matrix[string1.length()][string2.length()];
    }

    public static int matrixLevenshtein(String string1, String string2) {
        for (int i = 0; i < string1.length() + 1; i++) {
            for (int j = 0; j < string2.length() + 1; j++) {
                if (i == 0 && j == 0) {
                    matrix[i][j] = 0;
                }
                if (i == 0 && j > 0) {
                    matrix[i][j] = j;
                }
                if (j == 0 && i > 0) {
                    matrix[i][j] = i;
                }
                if (i > 0 && j > 0) {
                    int cost = (string1.charAt(i - 1) == string2.charAt(j - 1)) ? 0 : 1;
                    matrix[i][j] = Math.min(Math.min(matrix[i - 1][j] + 1, matrix[i][j - 1] + 1), matrix[i - 1][j - 1] + cost);
                }
            }
        }
        return matrix[string1.length()][string2.length()];
    }

    public static int matrixDamerauLevenshtein(String string1, String string2) {
        for (int i = 0; i < string1.length() + 1; i++) {
            for (int j = 0; j < string2.length() + 1; j++) {
                if (Math.min(i, j) == 0) {
                    matrix[i][j] = Math.max(i, j);
                } else {
                    int cost = (string1.charAt(i - 1) == string2.charAt(j - 1)) ? 0 : 1;
                    matrix[i][j] = Math.min(Math.min(matrix[i][j - 1] + 1, matrix[i - 1][j] + 1),
                            matrix[i - 1][j - 1] + cost);
                    if (j > 1 && i > 1 && string1.charAt(i - 1) == string2.charAt(j - 2) &&
                            string1.charAt(i - 2) == string2.charAt(j - 1)) {
                        matrix[i][j] = Math.min(matrix[i][j], matrix[i - 2][j - 2] + 1);
                    }
                }
            }
        }
        return matrix[string1.length()][string2.length()];
    }


    public static void matrixInit(int rows, int columns) {
        matrix = new Integer[rows + 1][columns + 1];
        for (int i = 0; i < rows + 1; i++) {
            for (int j = 0; j < columns + 1; j++) {
                if (i == 0 && j == 0) {
                    matrix[i][j] = 0;
                }
                if (i == 0 && j > 0) {
                    matrix[i][j] = j;
                }
                if (j == 0 && i > 0) {
                    matrix[i][j] = i;
                }
            }
        }
    }

    public static void matrixPrint(String string1, String string2) {
        System.out.print("\n");
        int k = 0;
        for (int i = 0; i < string1.length() + 1; i++) {
            if (i == 0) {
                System.out.print("    ");
                for (int j = 0; j < string2.length(); j++) {
                    System.out.print(string2.charAt(j) + " ");
                }
                System.out.print("\n");
            }
            for (int j = 0; j < string2.length() + 1; j++) {
                if (j == 0 && j < string1.length()) {
                    if (i == 0) {
                        System.out.print("  ");
                    } else {
                        System.out.print(string1.charAt(k) + " ");
                        k++;
                    }

                }
                System.out.print(matrix[i][j] + " ");
            }
            System.out.print("\n");
        }
    }

}

public class Main {

    private static long timer(Runnable method, TimeUnit timeUnit) {
        long time = System.nanoTime();
        method.run();
        time = System.nanoTime() - time;
        return TimeUnit.NANOSECONDS.convert(time, timeUnit);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первую строку");
        String s1 = scanner.next();
        System.out.println("Введите вторую строку");
        String s2 = scanner.next();
        Distance.matrixInit(s1.length(), s2.length());
        System.out.println("\nРезультаты работы алгоритмов:");
        System.out.println("Матричный Левенштейн: " + Distance.matrixLevenshtein(s1, s2) + "\n"
                + "Рекурсивный Левенштейн: " + Distance.recursiveLevenshtein(s1, s2) + "\n"
                + "Рекурсивный Левенштейн с заполнением матрицы: " + Distance.recursiveMatrixLevenshtein(s1, s2) + "\n"
                + "Матричный Дамерау-Левенштейн: " + Distance.matrixDamerauLevenshtein(s1, s2) + "\n");


        long timeMatrixLevenshtein = timer(() -> {
            for (int i = 0; i < 100; i++) {
                Distance.matrixInit(s1.length(), s2.length());
                Distance.matrixLevenshtein(s1, s2);
            }
        }, TimeUnit.NANOSECONDS);


        long timeRecursiveLevenshtein = timer(() -> {
            for (int i = 0; i < 100; i++) {
                Distance.matrixInit(s1.length(), s2.length());
                Distance.recursiveLevenshtein(s1, s2);
            }
        }, TimeUnit.NANOSECONDS);

        long timeRecursiveMatrixLevenshtein = timer(() -> {
            for (int i = 0; i < 100; i++) {
                Distance.matrixInit(s1.length(), s2.length());
                Distance.recursiveMatrixLevenshtein(s1, s2);
            }
        }, TimeUnit.NANOSECONDS);

        long timeMatrixDamerauLevenshtein = timer(() -> {
            for (int i = 0; i < 100; i++) {
                Distance.matrixInit(s1.length(), s2.length());
                Distance.matrixDamerauLevenshtein(s1, s2);
            }
        }, TimeUnit.NANOSECONDS);

        System.out.println("Среднее время работы алгоритмов за 100 итераций:");
        System.out.print("Матричный Левенштейн: " + timeMatrixLevenshtein / 100 + "\n"
                + "Рекурсивный Левенштейн: " + timeRecursiveLevenshtein / 100 + "\n"
                + "Рекурсивный Левенштейн с заполнением матрицы: " + timeRecursiveMatrixLevenshtein / 100 + "\n"
                + "Матричный Дамерау-Левенштейн: " + timeMatrixDamerauLevenshtein / 100 + "\n");

        System.out.print("\n Матрица Левенштейна \n");
        Distance.matrixInit(s1.length(), s2.length());
        Distance.matrixLevenshtein(s1, s2);
        Distance.matrixPrint(s1, s2);

        System.out.print("\n Рекурсивная матрица Левенштейна \n");
        Distance.matrixInit(s1.length(), s2.length());
        Distance.recursiveMatrixLevenshtein(s1, s2);
        Distance.matrixPrint(s1, s2);

        System.out.print("\n Матрица Дамерау-Левенштейна \n");
        Distance.matrixInit(s1.length(), s2.length());
        Distance.matrixDamerauLevenshtein(s1, s2);
        Distance.matrixPrint(s1, s2);
    }
}


