package com.company;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.lang.String;

class Matrix {
    private static Integer[][] matrixFirst = null;

    private static Integer[][] matrixSecond = null;

    private static Integer[][] matrixFinal = null;

    private static Integer[] rowFactors = null;

    private static Integer[] columnFactors = null;

    private static int rowsFirstMatrix;

    private static int columnsFirstMatrix;

    private static int rowsSecondMatrix;

    private static int columnsSecondMatrix;

    public static void matrixInit() {
        System.out.print("\nВведите размер первой матрицы:");
        Scanner scanner = new Scanner(System.in);
        rowsFirstMatrix = scanner.nextInt();
        columnsFirstMatrix = scanner.nextInt();
        matrixFirst = new Integer[rowsFirstMatrix + 1][columnsFirstMatrix + 1];
        System.out.print("\nВведите содержимое первой матрицы:\n");
        for (int i = 1; i <= rowsFirstMatrix; i++) {
            for (int j = 1; j <= columnsFirstMatrix; j++) {
                int variable = scanner.nextInt();
                matrixFirst[i][j] = variable;
            }
        }
        System.out.print("\nВведите размер второй матрицы:");
        rowsSecondMatrix = scanner.nextInt();
        columnsSecondMatrix = scanner.nextInt();
        if (columnsFirstMatrix != rowsSecondMatrix) {
            System.out.println("Ошибка! Число столбцов первой матрицы не равно числу строк второй матрицы!");
            System.exit(-1);
        }
        matrixSecond = new Integer[rowsSecondMatrix + 1][columnsSecondMatrix + 1];
        System.out.print("\nВведите содержимое второй матрицы:\n");
        for (int i = 1; i <= rowsSecondMatrix; i++) {
            for (int j = 1; j <= columnsSecondMatrix; j++) {
                int variable = scanner.nextInt();
                matrixSecond[i][j] = variable;
            }
        }

    }

    public static void matrixPrint(boolean full) {
        if (full) {
            System.out.println("Первая матрица:\n");
            for (int i = 1; i <= rowsFirstMatrix; i++) {
                for (int j = 1; j <= columnsFirstMatrix; j++) {
                    System.out.print(matrixFirst[i][j] + "\t");
                }
                System.out.println();
            }
            System.out.println("\nВторая матрица:\n");
            for (int i = 1; i <= rowsSecondMatrix; i++) {
                for (int j = 1; j <= columnsSecondMatrix; j++) {
                    System.out.print(matrixSecond[i][j] + "\t");
                }
                System.out.println();
            }
        }
        System.out.println("\nФинальная матрица:\n");
        for (int i = 1; i <= rowsFirstMatrix; i++) {
            for (int j = 1; j <= columnsSecondMatrix; j++) {
                System.out.print(matrixFinal[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public static void matrixClassicMultiplication() {

        matrixFinal = new Integer[rowsFirstMatrix + 1][columnsSecondMatrix + 1];
        for (int i = 1; i <= rowsFirstMatrix; i++) {
            for (int j = 1; j <= columnsSecondMatrix; j++) {
                matrixFinal[i][j] = 0;
                for (int k = 1; k <= columnsFirstMatrix; k++) {
                    matrixFinal[i][j] += matrixFirst[i][k] * matrixSecond[k][j];
                }
            }
        }

    }

    public static void matrixVinogradMultiplication() {
        matrixFinal = new Integer[rowsFirstMatrix + 1][columnsSecondMatrix + 1];
        rowFactors = new Integer[rowsFirstMatrix + 1];
        columnFactors = new Integer[columnsSecondMatrix + 1];
        //G[a][b] на H[b][c]
        int d = columnsFirstMatrix / 2;
        for (int i = 1; i <= rowsFirstMatrix; i++) {
            rowFactors[i] = matrixFirst[i][1] * matrixFirst[i][2];
            for (int j = 2; j <= d; j++) {
                rowFactors[i] = rowFactors[i] + matrixFirst[i][2 * j - 1] * matrixFirst[i][2 * j];
            }
        }
        for (int i = 1; i <= columnsSecondMatrix; i++) {
            columnFactors[i] = matrixSecond[1][i] * matrixSecond[2][i];
            for (int j = 2; j <= d; j++) {
                columnFactors[i] = columnFactors[i] + matrixSecond[2 * j - 1][i] * matrixSecond[2 * j][i];
            }
        }
        for (int i = 1; i <= rowsFirstMatrix; i++) {
            for (int j = 1; j <= columnsSecondMatrix; j++) {
                matrixFinal[i][j] = -rowFactors[i] - columnFactors[j];
                for (int k = 1; k <= d; k++) {
                    matrixFinal[i][j] = matrixFinal[i][j] + (matrixFirst[i][2 * k - 1] + matrixSecond[2 * k][j])
                            * (matrixFirst[i][2 * k] + matrixSecond[2 * k - 1][j]);
                }
            }
        }
        if (columnsFirstMatrix % 2 != 0) {
            for (int i = 1; i <= rowsFirstMatrix; i++) {
                for (int j = 1; j <= columnsSecondMatrix; j++) {
                    matrixFinal[i][j] = matrixFinal[i][j] + matrixFirst[i][columnsFirstMatrix]
                            * matrixSecond[rowsSecondMatrix][j];
                }
            }
        }
    }

    public static void matrixVinogradMultiplicationOptimized() {
        matrixFinal = new Integer[rowsFirstMatrix + 1][columnsSecondMatrix + 1];
        rowFactors = new Integer[rowsFirstMatrix + 1];
        columnFactors = new Integer[columnsSecondMatrix + 1];
        boolean odd = (columnsFirstMatrix % 2 != 0);
        //G[a][b] на H[b][c]
        int d = columnsFirstMatrix >> 1;
        for (int i = 1; i <= rowsFirstMatrix; i++) {
            rowFactors[i] = matrixFirst[i][1] * matrixFirst[i][2];
            for (int j = 2; j <= d; j++) {
                rowFactors[i] = rowFactors[i] + matrixFirst[i][2 * j - 1] * matrixFirst[i][2 * j];
            }
        }
        for (int i = 1; i <= columnsSecondMatrix; i++) {
            columnFactors[i] = matrixSecond[1][i] * matrixSecond[2][i];
            for (int j = 2; j <= d; j++) {
                columnFactors[i] = columnFactors[i] + matrixSecond[2 * j - 1][i] * matrixSecond[2 * j][i];
            }
        }
        for (int i = 1; i <= rowsFirstMatrix; i++) {
            for (int j = 1; j <= columnsSecondMatrix; j++) {
                matrixFinal[i][j] = -rowFactors[i] - columnFactors[j];
                for (int k = 1; k <= d; k++) {
                    matrixFinal[i][j] = matrixFinal[i][j] + (matrixFirst[i][2 * k - 1] + matrixSecond[2 * k][j])
                            * (matrixFirst[i][2 * k] + matrixSecond[2 * k - 1][j]);
                }
                if (odd) {
                    matrixFinal[i][j] = matrixFinal[i][j] + matrixFirst[i][columnsFirstMatrix]
                            * matrixSecond[rowsSecondMatrix][j];
                }
            }
        }
    }

    public static void printObjectSize() {
        System.out.println("Размер затраченной памяти в битах: " + Integer.SIZE / 8 * (rowsFirstMatrix * columnsFirstMatrix
                + rowsSecondMatrix * columnsSecondMatrix + rowsFirstMatrix * columnsSecondMatrix + rowsFirstMatrix
                + columnsSecondMatrix + rowsFirstMatrix + columnsSecondMatrix + 4));
    }
    /*
    public static long matrixTimeTest() {
        ThreadMXBean threadMX = ManagementFactory.getThreadMXBean();
        threadMX.setThreadContentionMonitoringEnabled(true);
        Thread thread = Thread.currentThread();
        long threadID = thread.getId();
        System.out.println(threadID);
        long cpuTime = threadMX.getThreadCpuTime(threadID);
        System.out.println(cpuTime);
        return cpuTime;
    }*/
}

public class Main {

    private static long timer(Runnable method, TimeUnit timeUnit) {
        long time = System.nanoTime();
        method.run();
        time = System.nanoTime() - time;
        return TimeUnit.NANOSECONDS.convert(time, timeUnit);
    }


    public static void main(String[] args) {


        Matrix.matrixInit();

        Matrix.matrixClassicMultiplication();

        System.out.println("\nКлассическое умножение:");

        Matrix.matrixPrint(false);

        Matrix.matrixVinogradMultiplication();

        System.out.println("\nВиноград:");

        Matrix.matrixPrint(false);

        Matrix.matrixVinogradMultiplicationOptimized();

        System.out.println("\nВиноград-оптимизированный:");

        Matrix.matrixPrint(false);

        System.out.println("\nЗамер процессерного времени работы алгоритмов:\n");

        long timeMatrixClassicMultiplication = timer(() -> {
            for (int i = 0; i < 1000000; i++) {
                Matrix.matrixClassicMultiplication();
            }
        }, TimeUnit.NANOSECONDS);

        long timeMatrixVinogradMultiplication = timer(() -> {
            for (int i = 0; i < 1000000; i++) {
                Matrix.matrixVinogradMultiplication();
            }
        }, TimeUnit.NANOSECONDS);

        long timeMatrixVinogradMultiplicationOptimized = timer(() -> {
            for (int i = 0; i < 1000000; i++) {
                Matrix.matrixVinogradMultiplicationOptimized();
            }
        }, TimeUnit.NANOSECONDS);

        System.out.print("Классическое умножение: " + timeMatrixClassicMultiplication / 1000000 + "\n"
                + "Виноград:: " + timeMatrixVinogradMultiplication / 1000000 + "\n"
                + "Оптимизированный Виноград: " +
                timeMatrixVinogradMultiplicationOptimized / 1000000 + "\n");

        Matrix.printObjectSize();


    }
}