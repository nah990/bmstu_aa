package com.company;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.lang.String;
import java.util.Random;

class ArraySort {
    private static Integer[] array = null;
    private static int arraySize = 0;

    public static int arraySizeGetter() {
        return arraySize;
    }

    public static void arraySizeSetter(int size) {
        arraySize = size;
    }

    public static void arrayInit() {
        System.out.print("Введите размер массива: ");
        Scanner scanner = new Scanner(System.in);
        arraySize = scanner.nextInt();
        array = new Integer[arraySize];
        System.out.print("Введите элементы массива: ");
        for (int i = 0; i < arraySize; i++) {
            array[i] = scanner.nextInt();
        }
    }

    public static void arrayInitRandom() {
        final Random random = new Random();
        array = new Integer[arraySize];
        for (int i = 0; i < arraySize; i++) {
            array[i] = random.nextInt();
        }
    }

    public static void arrayReverse() {
        for (int i = 0; i < arraySize / 2; i++) {
            int temp = array[i];
            array[i] = array[arraySize - 1 - i];
            array[arraySize - 1 - i] = temp;
        }
    }

    public static void arrayOutput() {
        System.out.print("Массив размером " + arraySize + " содержит следующие элементы:");
        for (int i = 0; i < arraySize; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("");
    }

    public static void swap(int firstPos, int secondPos) {
        int temp = array[firstPos];
        array[firstPos] = array[secondPos];
        array[secondPos] = temp;
    }

    public static void bubbleSort() {
        for (int i = 0; i < arraySize - 1; i++) {
            for (int j = 0; j < arraySize - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    swap(j + 1, j);
                }
            }
        }
    }

    public static void insertionSort() {
        for (int i = 1; i < arraySize; i++) {
            int key = array[i];
            int j = i - 1;
            while (j >= 0 && array[j] > key) {
                array[j + 1] = array[j];
                j = j - 1;
            }
            array[j + 1] = key;
        }
    }

    public static void selectionSort() {
        for (int i = 0; i < arraySize - 1; i++) {
            int min_id = i;
            for (int j = i + 1; j < arraySize; j++) {
                if (array[j] < array[min_id]) {
                    min_id = j;
                }
            }
            swap(min_id, i);
        }
    }
}

class Testing {

    private static Integer numOfTimeMeasurements = null;
    private static Integer arraySize = null;
/*
    public static long timeTest() {
        ThreadMXBean threadMX = ManagementFactory.getThreadMXBean();
        threadMX.setThreadContentionMonitoringEnabled(true);
        Thread thread = Thread.currentThread();
        long threadID = thread.getId();
        System.out.println(threadID);
        long cpuTime = threadMX.getThreadCpuTime(threadID);
        System.out.println(cpuTime);
        return cpuTime;
    }
*/
    private static long timer(Runnable method, TimeUnit timeUnit) {
        long time = System.nanoTime();
        method.run();
        time = System.nanoTime() - time;
        return TimeUnit.NANOSECONDS.convert(time, timeUnit);
    }

    public static void setTestingOptions() {
        System.out.println("Введите количество замеров времени: ");
        Scanner scanner = new Scanner(System.in);
        numOfTimeMeasurements = scanner.nextInt();
        System.out.println("Введите размер массива для замера времени: ");
        arraySize = scanner.nextInt();
    }

    public static long bubbleSortTime(int sortCase) {
        long finalTimeBubbleSort = 0;
        for (int i = 0; i < numOfTimeMeasurements; i++) {
            ArraySort.arrayInitRandom();
            if (sortCase == 1) {
                ArraySort.bubbleSort();
                ArraySort.arrayReverse();
            } else {
                if (sortCase == 2) {
                    ArraySort.bubbleSort();
                }
            }
            long timeBubbleSort = timer(ArraySort::bubbleSort, TimeUnit.NANOSECONDS);
            finalTimeBubbleSort += timeBubbleSort;
        }
        return finalTimeBubbleSort / numOfTimeMeasurements;
    }

    public static long insertionSortTime(int sortCase) {
        long finalTimeInsertionSort = 0;
        for (int i = 0; i < numOfTimeMeasurements; i++) {
            ArraySort.arrayInitRandom();
            if (sortCase == 1) {
                ArraySort.insertionSort();
                ArraySort.arrayReverse();
            } else {
                if (sortCase == 2) {
                    ArraySort.insertionSort();
                }
            }
            long timeInsertionSort = timer(ArraySort::insertionSort, TimeUnit.NANOSECONDS);
            finalTimeInsertionSort += timeInsertionSort;
        }
        return finalTimeInsertionSort / numOfTimeMeasurements;
    }

    public static long selectionSortTime(int sortCase) {
        long finalTimeSelectionSort = 0;
        for (int i = 0; i < numOfTimeMeasurements; i++) {
            ArraySort.arrayInitRandom();
            if (sortCase == 1) {
                ArraySort.selectionSort();
                ArraySort.arrayReverse();
            } else {
                if (sortCase == 2) {
                    ArraySort.arrayReverse();
                    ArraySort.selectionSort();
                }
            }
            long timeSelectionSort = timer(ArraySort::selectionSort, TimeUnit.NANOSECONDS);
            finalTimeSelectionSort += timeSelectionSort;
        }
        return finalTimeSelectionSort / numOfTimeMeasurements;
    }

    public static void startTesting() {
        setTestingOptions();
        ArraySort.arraySizeSetter(arraySize);
        System.out.print("Процесс сортироворки \n|         |\n|");
        long b1 = bubbleSortTime(0);
        System.out.print("-");
        long b2 = bubbleSortTime(1);
        System.out.print("-");
        long b3 = bubbleSortTime(2);
        System.out.print("-");
        long i1 = insertionSortTime(0);
        System.out.print("-");
        long i2 = insertionSortTime(1);
        System.out.print("-");
        long i3 = insertionSortTime(2);
        System.out.print("-");
        long s1 = selectionSortTime(0);
        System.out.print("-");
        long s2 = selectionSortTime(1);
        System.out.print("-");
        long s3 = selectionSortTime(2);
        System.out.print("-|\n");
        System.out.println("Среднее время сортировки массива на " + arraySize +
                " элементов за " + numOfTimeMeasurements + " повторений в наносекундах");
        System.out.format("\t\t\t\t\t\t\t\tСлучайный случай\t\t\tХудший случай\t\t\tЛучший случай\n"
                        + "Пузырьковая сортировка:\t\t\t%8d\t\t\t\t\t%8d\t\t\t%8d\n"
                        + "Сортировка вставками:   \t\t%8d\t\t\t\t\t%8d\t\t\t%8d\n"
                        + "Сортировка выбором:     \t\t%8d\t\t\t\t\t%8d\t\t\t%8d\n",
                b2,b1,b3,i1,i2,i3,s2,s1,s3);

    }
}


public class Main {

    public static void main(String[] args) {
        /*ArraySort.arraySizeSetter(10);
        ArraySort.arrayInitRandom();*/
        ArraySort.arrayInit();
        System.out.println("Введенный массив");
        ArraySort.arrayOutput();
        ArraySort.insertionSort();
        System.out.println("Отсортированный массив");
        ArraySort.arrayOutput();
        ArraySort.arrayReverse();
        System.out.println("Отсортированный в обратном порядке массив");
        ArraySort.arrayOutput();
        Testing.startTesting();
        //10 9 2 4 1 3 5 7 6 8
    }
}
